package in.abhishekkumarsingh.blog.service;

import java.util.List;

import in.abhishekkumarsingh.blog.payload.CommentDto;

public interface CommentService {

	CommentDto createComment(long id, CommentDto commentDto);
	
	List<CommentDto> getCommentsByPostId(long postId);
	
	CommentDto getCommentById(long postId,long commentId);
	
	CommentDto updateComment(Long postId, Long commentId, CommentDto commentRequest);
	
	void deleteComment(Long postId, Long commentId);
	
}
