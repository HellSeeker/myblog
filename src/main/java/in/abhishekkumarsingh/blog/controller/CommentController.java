package in.abhishekkumarsingh.blog.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.abhishekkumarsingh.blog.payload.CommentDto;
import in.abhishekkumarsingh.blog.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="CRUD REST API's for create comment on Posts")
@RestController
@RequestMapping("/api/v1/posts/")
public class CommentController {

	private CommentService commnetService;

	public CommentController(CommentService commnetService) {
		this.commnetService = commnetService;
	}

	@ApiOperation(value="Create Comment REST API")
	@PostMapping("/{postId}/comments")
	public ResponseEntity<CommentDto> createComment(@PathVariable(name = "postId") long id,
			@Valid @RequestBody CommentDto commentDto) {

		return new ResponseEntity<>(commnetService.createComment(id, commentDto), HttpStatus.CREATED);
	}
	
	@ApiOperation(value="Get Comment by post id REST API")
	@GetMapping("/{postId}/comments")
	public List<CommentDto> getCommentsByPostId(@PathVariable(name = "postId") long postId) {

		return commnetService.getCommentsByPostId(postId);
	}

	@ApiOperation(value="Get Comment by id REST API")
	@GetMapping("/{postId}/comments/{id}")
	public ResponseEntity<CommentDto> getCommentById(@PathVariable(name = "postId") Long postId,
			@PathVariable(name = "id") Long commentId) {

		CommentDto commnetDto = commnetService.getCommentById(postId, commentId);

		return new ResponseEntity<>(commnetDto, HttpStatus.OK);
	}

	@ApiOperation(value="Update Comment by id REST API")
	@PutMapping("/{postId}/comments/{id}")
	public ResponseEntity<CommentDto> updateComment(@PathVariable(name = "postId") Long postId,
			@PathVariable(name = "id") Long commentId, @Valid @RequestBody CommentDto commentRequest) {

		CommentDto updatedComment = commnetService.updateComment(postId, commentId, commentRequest);

		return new ResponseEntity<CommentDto>(updatedComment, HttpStatus.OK);
	}

	@ApiOperation(value="Delete Comment by id REST API")
	@DeleteMapping("/{postId}/comments/{id}")
	public ResponseEntity<String> updateComment(@PathVariable(name = "postId") Long postId,
			@PathVariable(name = "id") Long commentId) {

		commnetService.deleteComment(postId, commentId);

		return new ResponseEntity<>("comment deleted sucessfully", HttpStatus.OK);
	}
}
