package in.abhishekkumarsingh.blog.controller;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.abhishekkumarsingh.blog.entity.Role;
import in.abhishekkumarsingh.blog.entity.User;
import in.abhishekkumarsingh.blog.payload.JwtAuthResponse;
import in.abhishekkumarsingh.blog.payload.LoginDto;
import in.abhishekkumarsingh.blog.payload.SignupDto;
import in.abhishekkumarsingh.blog.repository.RoleRepository;
import in.abhishekkumarsingh.blog.repository.UserRepository;
import in.abhishekkumarsingh.blog.security.JwtTokenProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(value="Auth controller exposes signin and signup REST API's")
@RestController
@RequestMapping("/api/v1/auth")
@Slf4j
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@ApiOperation(value="REST API for Login to Blog API")
	@PostMapping("/signin")
	public ResponseEntity<JwtAuthResponse> authenticateUser(@RequestBody LoginDto loginDto){
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsernameOrEmail(), loginDto.getPassword()));
		log.info("Authentication object=========>"+authentication.toString());
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		// get token form token provider class
		String token = jwtTokenProvider.generateToken(authentication);
		
		return ResponseEntity.ok(new JwtAuthResponse(token));
	}
	
	@ApiOperation(value="REST API to register to Blog API")
	@PostMapping("/signup")
	public ResponseEntity<String> authenticateUser(@RequestBody SignupDto signupDto){
		
		if(userRepository.existsByUsername(signupDto.getUsername())) {
			return new ResponseEntity<>("Username already exists",HttpStatus.BAD_REQUEST);
		}
		
		if(userRepository.existsByEmail(signupDto.getEmail())) {
			return new ResponseEntity<>("Email already exists",HttpStatus.BAD_REQUEST);
		}
		
		User user = new User();
		user.setName(signupDto.getName());
		user.setUsername(signupDto.getUsername());
		user.setEmail(signupDto.getEmail());
		user.setPassword(passwordEncoder.encode(signupDto.getPassword()));
		
		Role roles = roleRepository.findByName("ROLE_ADMIN").get();
		user.setRoles(Collections.singleton(roles));
		userRepository.save(user);
		
		return new ResponseEntity<>("User Registred successfully!!",HttpStatus.OK);
	}
	
}
