package in.abhishekkumarsingh.blog.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CommentDto {
	private Long id;
	
	@NotEmpty(message="Name should not be null or Empty")
	private String name;
	
	@NotEmpty(message="Email should not be null or Empty")
	@Email
	private String email;
	
	@NotEmpty
	@Size(min=10, message="Body should have atleast 10 characters")
	private String body;
}
