package in.abhishekkumarsingh.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.abhishekkumarsingh.blog.entity.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
	

}
