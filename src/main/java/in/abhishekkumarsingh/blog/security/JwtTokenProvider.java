package in.abhishekkumarsingh.blog.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import in.abhishekkumarsingh.blog.exception.BlogApiException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

@Component
public class JwtTokenProvider {

	@Value("${app.jwt-secret}")
	private String jwtSecret;
	@Value("${app.jwt-expiration-milliseconds}")
	private int jwtExpirationInMs;

	public String generateToken(Authentication authentication) {
		String username = authentication.getName();
		Date currentDate = new Date();
		Date expireDate = new Date(currentDate.getTime() + jwtExpirationInMs);
		String token = Jwts.builder().setSubject(username).setIssuedAt(new Date()).setExpiration(expireDate)
				.signWith(Keys.hmacShaKeyFor(jwtSecret.getBytes()),SignatureAlgorithm.HS512).compact();
		return token;
	}

	// get username from token
	public String getUsernameFromJWT(String token) {
		
//		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		Claims claims = Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(jwtSecret.getBytes())).build().parseClaimsJws(token).getBody();
		return claims.getSubject();
	}
	
	// validate jwt token
	public boolean validateToken(String token) {
		try {
			Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(jwtSecret.getBytes())).build().parseClaimsJws(token);
			return true;
		}catch(SignatureException ex) {
			throw new BlogApiException(HttpStatus.BAD_REQUEST, "invalid JWT signature");
		}catch(MalformedJwtException ex) {
			throw new BlogApiException(HttpStatus.BAD_REQUEST, "invalid JWT token");
		}catch(ExpiredJwtException ex) {
			throw new BlogApiException(HttpStatus.BAD_REQUEST, "invalid JWT expired");
		}catch(UnsupportedJwtException ex) {
			throw new BlogApiException(HttpStatus.BAD_REQUEST, "unsupported JWT token");
		}catch(IllegalArgumentException ex) {
			throw new BlogApiException(HttpStatus.BAD_REQUEST, "JWt Claims String is empty");
		}
	}
}
